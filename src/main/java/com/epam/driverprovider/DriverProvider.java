package com.epam.driverprovider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

import static com.epam.utils.Constants.*;


public class DriverProvider {

    public static final int IMPLICITLY_TIME = 10;
    public static final ThreadLocal<WebDriver> DRIVER_POOL = new ThreadLocal<>();
    private static final Logger log = LogManager.getLogger(DriverProvider.class);

    protected static WebDriver createDriver(String browser) {
        WebDriver driver;
        log.info("try to create driver " + browser);

        System.setProperty(DRIVER, DRIVER_PATH);
        driver = new ChromeDriver(getChromeOptionsAndCapabilities());

        driver.manage().timeouts().implicitlyWait(IMPLICITLY_TIME, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        log.info(browser + " driver was successfully created");
        return driver;
    }

    private static ChromeOptions getChromeOptionsAndCapabilities() {
        DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
        chromeCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        chromeCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                UnexpectedAlertBehaviour.ACCEPT);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        chromeOptions.merge(chromeCapabilities);
        return chromeOptions;
    }

    public static WebDriver getDriver() {
        if (DRIVER_POOL.get() == null) {
            DRIVER_POOL.set(DriverProvider.createDriver(BROWSER));
        }
        return DRIVER_POOL.get();
    }

    public static void quitDriver() {
        log.info("try to quit driver");
        if (DRIVER_POOL.get() != null) {
            DRIVER_POOL.get().quit();
            DRIVER_POOL.set(null);
        }
        log.info("driver was successfully closed");
    }
}
