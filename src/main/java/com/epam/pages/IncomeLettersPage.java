package com.epam.pages;

import com.epam.decorator.elementsImpl.Button;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IncomeLettersPage extends BasePage {

    public IncomeLettersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[contains(@class, 'gb_La gbii')]")
    private WebElement userImg;

    @FindBy(xpath = "//*[contains(@class, 'gb_vb')]")
    private WebElement userEmail;

    @FindBy(xpath = "//*[@class='z0']//div")
    private Button composeBtn;

    @FindBy(xpath = "//div[@class='gb_Bc' and @role='button']")
    private Button burgerMenuBtn;

    public WebElement getUserImg() {
        return userImg;
    }

    public WebElement getEmail() {
        return userEmail;
    }

    public Button getComposeBtn() {
        return composeBtn;
    }

    public Button getBurgerMenuBtn() {
        return burgerMenuBtn;
    }
}
