package com.epam.pages;

import com.epam.decorator.elementsImpl.Button;
import com.epam.decorator.elementsImpl.Input;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MessagePage extends BasePage {

    public MessagePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//textarea[@role='combobox']")
    private WebElement receiverField;

    @FindBy(css = "input.aoT[name='subjectbox']")
    private Input subjectField;

    @FindBy(css = "div[role='textbox']")
    private WebElement messageField;

    @FindBy(xpath = "//div[@role='button' and @class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
    private Button sendBtn;

    @FindBy(css = ".vh > .aT")
    private WebElement popUp;

    public Button getSendBtn() {
        return sendBtn;
    }

    public WebElement getReceiverField() {
        return receiverField;
    }

    public Input getSubjectField() {
        return subjectField;
    }

    public WebElement getMessageField() {
        return messageField;
    }

    public WebElement getPopUp() {
        return popUp;
    }
}
