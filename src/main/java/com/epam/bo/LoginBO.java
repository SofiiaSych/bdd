package com.epam.bo;

import com.epam.pages.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;


public class LoginBO {
    private LoginPage loginPage;
    private static final Logger log = LogManager.getLogger(LoginBO.class);

    public LoginBO(WebDriver driver) {
        loginPage = new LoginPage(driver);
    }

    public void typeLoginPasswordAndSubmit(String login, String password) {
        log.info("typing login and password");
        loginPage.getLoginField().sendKeys(login + Keys.ENTER);
        loginPage.getPasswdField().sendKeys(password + Keys.ENTER);
    }
}
