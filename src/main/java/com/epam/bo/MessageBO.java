package com.epam.bo;

import com.epam.pages.MessagePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class MessageBO {
    private MessagePage messagePage;
    private static final Logger log = LogManager.getLogger(MessageBO.class);


    public MessageBO(WebDriver driver) {
        messagePage = new MessagePage(driver);
    }

    public void fillMessage(String receiver, String subject, String message) {
        log.info("filling message");
        messagePage.getReceiverField().sendKeys(receiver);
        messagePage.getSubjectField().sendKeys(subject);
        messagePage.getMessageField().sendKeys(message);
    }

    public void sendMessage() {
        log.info("sending message");
        messagePage.getSendBtn().click();
    }

    public boolean checkSubjectFieldIsDisplayed() {
        log.info("checking if subject field is displayed");
        return messagePage.getSubjectField().isDisplayed();
    }

    public boolean checkPopUpIsDisplayed() {
        log.info("checking is pop-up is displayed ");
        return messagePage.getPopUp().isDisplayed();
    }


}
