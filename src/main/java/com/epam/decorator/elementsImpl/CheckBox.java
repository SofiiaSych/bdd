package com.epam.decorator.elementsImpl;

import com.epam.decorator.AbstractElement;
import org.openqa.selenium.WebElement;


public class CheckBox extends AbstractElement {

    public CheckBox(WebElement element) {
        super(element);
    }

    public boolean isChecked() {
        return element.isSelected();
    }

    public void setChecked(boolean value) {
        if (isChecked() != value) {
            element.click();
        }
    }
}
