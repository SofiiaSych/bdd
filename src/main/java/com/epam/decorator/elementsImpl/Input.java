package com.epam.decorator.elementsImpl;

import com.epam.decorator.AbstractElement;
import org.openqa.selenium.WebElement;

public class Input extends AbstractElement {
    public Input(WebElement element) {
        super(element);
    }

    public void type(String input) {
        element.clear();
        element.sendKeys(input);
    }

}
