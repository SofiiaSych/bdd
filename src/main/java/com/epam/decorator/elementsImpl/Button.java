package com.epam.decorator.elementsImpl;

import com.epam.decorator.AbstractElement;
import com.epam.driverprovider.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Button extends AbstractElement {

    private static final int EXPLICITLY_TIME = 20;

    public Button(WebElement element) {
        super(element);
    }

    public void waitUntilClickableAndClick() {
        element = new WebDriverWait(DriverProvider.getDriver(), EXPLICITLY_TIME)
                .until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void waitUntilVisibleAndClick() {
        element = new WebDriverWait(DriverProvider.getDriver(), EXPLICITLY_TIME)
                .until(ExpectedConditions.visibilityOf(element));
        element.click();
    }
}
