package com.epam.utils;


public class Constants {

    public static final String RECEIVER = ConfigProperties.getProperty("receiver");
    public static final String SUBJECT = ConfigProperties.getProperty("subject");
    public static final String MESSAGE = ConfigProperties.getProperty("message");
    public static final String DRIVER_PATH = ConfigProperties.getProperty("chromedriver_path");
    public static final String DRIVER = ConfigProperties.getProperty("chromedriver");
    public static final String BROWSER = ConfigProperties.getProperty("browser");
    public static final String BASE_URL = ConfigProperties.getProperty("loginPage");
    public static final String USER_DATA = ConfigProperties.getProperty("userData");
}
