package com.epam.utils;

import com.epam.model.User;
import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static com.epam.utils.Constants.USER_DATA;
import static java.util.Arrays.asList;


public class ConfigProperties {

    protected static FileInputStream fileInputStream;
    protected static Properties PROPERTIES;


    static {
        try {
            fileInputStream = new FileInputStream("src/main/resources/conf.properties");
            PROPERTIES = new Properties();
            PROPERTIES.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }

    public static List<User> getCredentials() {
        List<User> user = asList();
        try {
            user = asList(new Gson().fromJson(new FileReader(USER_DATA), User[].class));
        } catch (FileNotFoundException ignored) {
        }
        return user;
    }

}

