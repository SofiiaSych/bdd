@parallel=true
Feature: Login gmail, select the last message and check if it is selected

Scenario Outline: 
	When User logs in to Gmail with email <email> and password <password>
	And User opens outcome messages
	And User selects the last message
	Then The last message should be selected
	Examples: 
		| email                         | password       |
		| "sychsofiiatest@gmail.com"  | "sofiia_test1" |
		| "sychsofiiatest1@gmail.com" | "sofiia_test1" |
		| "sychsofiiatest2@gmail.com" | "sofiia_test1" |
		| "sychsofiiatest3@gmail.com" | "sofiia_test1" |
		| "sychsofiiates4t@gmail.com" | "sofiia_test1" |
		
