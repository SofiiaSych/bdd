package epam.com;

import com.epam.driverprovider.DriverProvider;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterMethod;

@CucumberOptions(
        features = {"src/test/resources/cucumberGmail.feature"},
        glue = {"steps"}
)
public class GmailTestRunner extends AbstractTestNGCucumberTests {

    @AfterMethod
    public void afterMethod() {
        DriverProvider.quitDriver();
    }

}
