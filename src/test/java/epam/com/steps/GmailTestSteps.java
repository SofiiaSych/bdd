package epam.com.steps;

import com.epam.bo.IncomeLettersBO;
import com.epam.bo.LoginBO;
import com.epam.bo.MessageBO;
import com.epam.bo.OutcomeLettersBO;
import com.epam.driverprovider.DriverProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import static com.epam.driverprovider.DriverProvider.DRIVER_POOL;
import static com.epam.utils.Constants.BASE_URL;

public class GmailTestSteps {
    protected IncomeLettersBO incomeLettersBO;
    protected LoginBO loginPageBO;
    protected OutcomeLettersBO outcomeLettersBO;
    protected MessageBO messageBO;

    @BeforeMethod
    public void setUp() {
        DriverProvider.getDriver().get(BASE_URL);
        incomeLettersBO = new IncomeLettersBO(DRIVER_POOL.get());
        loginPageBO = new LoginBO(DRIVER_POOL.get());
        outcomeLettersBO = new OutcomeLettersBO(DRIVER_POOL.get());
        messageBO = new MessageBO(DRIVER_POOL.get());
    }

    @When("^User logs in to Gmail with email \"(.*)\"$ and password \"(.*)\"$")
    public void typeLoginPasswordAndSubmit(String email, String password) {
        loginPageBO.typeLoginPasswordAndSubmit(email, password);
        String userEmail = incomeLettersBO.getUserEmail();
        Assert.assertEquals(email, userEmail, "Incorrect user email");
    }

    @And("^User opens outcome messages")
    public void openOutcomeLetters() {
        outcomeLettersBO.openOutcomeLetters();
    }

    @And("^User selects the last message")
    public void selectLastMessage() {
        incomeLettersBO.openBurgerMenu();
        outcomeLettersBO.selectLastMessage();
    }

    @Then("^The last message should be selected")
    public void verifyLastMessageIsSelected() {
        Assert.assertTrue(outcomeLettersBO.checkLastMessageCheckBoxIsSelected(), "The letter isn't selected");
    }

}
